var options = {
    strings: ['Hi. I am in the cat\'s dreams, I\'m a 21 year old self-taught programmer. I started writing code almost 10 years ago now. My first projects revolved around game hacking, but as the years went by I discovered new languages and technologies, nowadays I mostly work with C/C++, C#, JavaScript and Python 3. In my spare time I like to code small utilities for myself and applications for Sony consoles.'],
    typeSpeed: 10
  };
  
  var typed = new Typed('#about-text', options);


  $(document).ready(function () {
    $(".popup").hide();
  });

  $(".minimize").click(function(){
    $(".popup").show();
  });

  $(".minimize-popup").click(function(){
    $(".popup").hide();
  });